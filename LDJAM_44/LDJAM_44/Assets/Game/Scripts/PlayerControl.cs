using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class PlayerControl : MonoBehaviour
{
	[Header("Character Controls")]
	CharacterController characterController;

    public float speed = 6.0f;
    public float jumpSpeed = 0f;
    public float gravity = 20.0f;
	public float rotationSpeed = 75.0f;

    [Header("Character")]
    public GameObject player;

    [Header("UI")]
	public Text winMessage;
	public Button restartBtn;
	public Image restartImg;
	public Text restartText;

    private Vector3 moveDirection;
	private float rotationY = 0f;
    private float rotationYForPlayer = 0f;
    private bool movementAllowed = true;
	private string controllerName="";
    private bool rotationNeed = false;


    void Start()
    {
        characterController = GetComponent<CharacterController>();
		if(winMessage.isActiveAndEnabled)
		{
			winMessage.enabled = false;
			restartBtn.enabled = false;
			restartImg.enabled = false;
			restartText.enabled = false;
		}
		movementAllowed = true;

		string[] names = Input.GetJoystickNames();
	     for (int x = 0; x < names.Length; x++)
	     {
	         print(names[x].Length);
	         if (names[x].Length == 19)
	         {
	             print("PS4 CONTROLLER IS CONNECTED");
				 controllerName = "PS4";
	         }
	         if (names[x].Length == 33)
	         {
	             print("XBOX ONE CONTROLLER IS CONNECTED");
				 controllerName = "XBOX";

	         }
	     }

    }

    void Update()
    {
        if (movementAllowed)
        {
            if (characterController.isGrounded)
            {
                // We are grounded, so recalculate
                // move direction directly from axes
                if (controllerName == "PS4")
                {
                    moveDirection = new Vector3(Input.GetAxis("Horizontal" + controllerName) * 0.5f, 0.0f, -Input.GetAxis("Vertical" + controllerName));
                }
                else
                {
                    moveDirection = new Vector3(Input.GetAxis("Horizontal" + controllerName) * 0.5f, 0.0f, Input.GetAxis("Vertical" + controllerName));
                }
                rotationY += Input.GetAxis("HorizontalRotation" + controllerName) * rotationSpeed * Time.deltaTime;
                rotationYForPlayer += Input.GetAxis("HorizontalRotation" + controllerName) * rotationSpeed * Time.deltaTime;
                Quaternion rotation = Quaternion.Euler(0, rotationY, 0);
                moveDirection = rotation * moveDirection;
                moveDirection *= speed;
                if (Input.GetAxis("Vertical" + controllerName) != 0 && rotationNeed)
                {
                    player.transform.Rotate(0, rotationYForPlayer, 0);
                    rotationNeed = false;
                    rotationYForPlayer = 0;
                }
            }

            // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
            // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
            // as an acceleration (ms^-2)
            moveDirection.y -= gravity * Time.deltaTime;

            player.transform.Rotate(0, -Input.GetAxis("HorizontalRotation" + controllerName) * Time.deltaTime * rotationSpeed, 0, Space.World);
            transform.Rotate(0, Input.GetAxis("HorizontalRotation" + controllerName) * Time.deltaTime * rotationSpeed, 0, Space.World);

            if (Input.GetAxis("HorizontalRotation" + controllerName) != 0)
            {
                rotationNeed = true;
            }


            // Move the controller
            characterController.Move(moveDirection * Time.deltaTime);
        }
        // Win the Game

        if (transform.position.x > 51 && transform.position.z > 51){
			winMessage.text = "You Win !!";
			winMessage.enabled = true;
			restartBtn.enabled = true;
			restartImg.enabled = true;
			restartText.enabled = true;
		}
		if(winMessage.isActiveAndEnabled){
			movementAllowed = false;
		}

    }
}
