﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public float moneyBySecond = 0.01f;

	[Header("UI")]
    public Image moneyFg;
	public Text winMessage;
	public Button restartBtn;
	public Image restartImg;
	public Text restartText;

    private string controllerName="";

    void Start(){
        Cursor.visible = false;
		if(winMessage.isActiveAndEnabled)
		{
			winMessage.enabled = false;
			restartBtn.enabled = false;
			restartImg.enabled = false;
			restartText.enabled = false;
		}
        moneyFg.fillAmount = 1f;

		string[] names = Input.GetJoystickNames();
	     for (int x = 0; x < names.Length; x++)
	     {
	         print(names[x].Length);
	         if (names[x].Length == 19)
	         {
	             print("PS4 CONTROLLER IS CONNECTED");
				 controllerName = "PS4";
	         }
	         if (names[x].Length == 33)
	         {
	             print("XBOX ONE CONTROLLER IS CONNECTED");
				 controllerName = "XBOX";

	         }
	     }
    }

    void Update(){
		if(Input.GetAxis("StartButton"+controllerName)>0){
	           RestartScene();
		}

        if(moneyFg.fillAmount == 0){
            winMessage.text = "You Lose !!";
			winMessage.enabled = true;
			restartBtn.enabled = true;
			restartImg.enabled = true;
			restartText.enabled = true;
        }
        if(!winMessage.isActiveAndEnabled){
            moneyFg.fillAmount -= moneyBySecond * Time.deltaTime;
        }
    }

    // Update is called once per frame
    public void RestartScene()
    {
        SceneManager.LoadScene(1);
    }
}
