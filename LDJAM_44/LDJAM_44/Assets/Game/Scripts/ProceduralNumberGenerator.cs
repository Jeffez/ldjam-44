﻿using UnityEngine;
using System.Collections;

public class ProceduralNumberGenerator {
	public static int currentPosition = 0;
	public const string key="1234";



	public static int GetNextNumber() {
		string currentNum = key.Substring(currentPosition++ % key.Length, 1);
		//return int.Parse (currentNum);
		return Mathf.RoundToInt(Random.Range(0.51f,5.49f));
	}
}
