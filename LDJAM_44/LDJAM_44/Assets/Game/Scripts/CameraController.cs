﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public int rotationSpeed=100;

    private string controllerName ="";

    // Start is called before the first frame update
    void Start()
    {
    	string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                controllerName = "PS4";
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                controllerName = "XBOX";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(controllerName == "XBOX"){
            if(transform.position.y > -1.5f && Input.GetAxis("VerticalRotation"+controllerName) < 0 ){
                transform.RotateAround(player.transform.position,player.transform.right,(Input.GetAxis("VerticalRotation"+controllerName)) * rotationSpeed * Time.deltaTime);
            }else if (transform.position.y < 1.5f && Input.GetAxis("VerticalRotation"+controllerName) > 0 ){
            transform.RotateAround(player.transform.position,player.transform.right,(Input.GetAxis("VerticalRotation"+controllerName)) * rotationSpeed * Time.deltaTime);
            }
        }else{
            if(transform.position.y > -1.5f && Input.GetAxis("VerticalRotation"+controllerName) > 0 ){
                transform.RotateAround(player.transform.position,player.transform.right,-(Input.GetAxis("VerticalRotation"+controllerName)) * rotationSpeed * Time.deltaTime);
            }else if (transform.position.y < 1.5f && Input.GetAxis("VerticalRotation"+controllerName) < 0 ){
                transform.RotateAround(player.transform.position,player.transform.right,-(Input.GetAxis("VerticalRotation"+controllerName)) * rotationSpeed * Time.deltaTime);
            }
        }

    }
}
