﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour
{

    private GameObject hitObject;

    // Start is called before the first frame update
    void Start()
    {
        hitObject = null;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, out hit))
        {
            if (hitObject != null)
            {
                if (!hitObject.Equals(hit.collider.gameObject))
                {
                    try
                    {
                        Color color = hitObject.GetComponent<MeshRenderer>().material.color;
                        color.a = 1.0f;
                        hitObject.GetComponent<MeshRenderer>().material.SetColor("_Color", color);
                    }
                    catch
                    {
                        print("We are watching to the player");
                    }
                }
            }
            if (hit.distance < 2)
            {
                Color color = hit.collider.gameObject.GetComponent<MeshRenderer>().material.color;
                color.a = 0.2f;
                hit.collider.gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", color);
            }
            hitObject = hit.collider.gameObject;
        }
    }
}
