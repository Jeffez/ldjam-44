﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChanger : MonoBehaviour
{
    public Image img;
    public Color color;
    // Start is called before the first frame update
    void Start()
    {
        color = Color.HSVToRGB(0.0f, 0.55f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        float H,S,V;
        Color.RGBToHSV(color, out H, out S, out V);
        if(H<1f){
            H+=0.0003f;
        }else{
            H=0.0f;
        }
        color = Color.HSVToRGB(H, S, V);
        img.color = color;
    }
}
