﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonMenu : MonoBehaviour
{

	[Header("UI")]
    public Image img;

    private int menu = 0;
    private string controllerName ="";
    private float savedTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
    	string[] names = Input.GetJoystickNames();
        for (int x = 0; x < names.Length; x++)
        {
            print(names[x].Length);
            if (names[x].Length == 19)
            {
                print("PS4 CONTROLLER IS CONNECTED");
                controllerName = "PS4";
            }
            if (names[x].Length == 33)
            {
                print("XBOX ONE CONTROLLER IS CONNECTED");
                controllerName = "XBOX";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton("Start"+controllerName)){
            Debug.Log("A");
            if(menu == 0){
                PlayScene();
            }
            if(menu==1){
                StopGame();
            }
        }
        if(Input.GetAxis("Vertical"+controllerName)<-0.7f && Time.time > savedTime+0.5f){
            menu++;
            img.transform.Translate(0,-150,0);
            savedTime = Time.time;
        }
        if(Input.GetAxis("Vertical"+controllerName)>0.7f && Time.time > savedTime+0.5f){
            menu--;
            img.transform.Translate(0,150,0);
            savedTime = Time.time;
        }
        if(menu <0){
            menu = 1;
            img.transform.Translate(0,-300,0);
        }
        if(menu > 1){
            menu = 0;
            img.transform.Translate(0,+300,0);
        }
    }

    public void PlayScene(){
        SceneManager.LoadScene(1);
    }

    public void StopGame(){
        Application.Quit();
    }
}
